#!/usr/bin/env python
#
""" ADS-B frame generator and GNU Radio .dat file converter

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""
__authors__ = ["Ruben Afonso", "Fabian Naf"]
__contact__ = "rubenafonso@tecnico.ulisboa.pt"
__copyright__ = "Copyright 2020, ISTsat-1"
__date__ = "2020/11/09"
__deprecated__ = False
__license__ = "GPLv3"
__maintainer__ = "developer"
__status__ = "Production"
__version__ = "1.0.0"
__email__ = ["rubenafonso@tecnico.ulisboa.pt", "fabian.naf@tecnico.ulisboa.pt"]
__credits__ = __authors__

import struct
import adbs_encoder
#import matplotlib.pyplot as plt


class AdsbFrame():
    """
    Class to represent a ADS-B Frame

    Attributes
    ----------
    icao : bytes
        ICAO aircraft address/Identifier
    lat: float
        Latitude of the sender
    lon: float
        Longitude of the sender
    alt: float
        Altitude of the sender
    ca: int
        Capability (additional identifier)
    tc: int
        Type code
    ss: int
        Surveillance status
    time: int
        Timebit of frame
    surface: boolean
        Surface or Airborne

    Methods
    -------

    """
    def __init__(self,
                 icao,
                 lat=0.0,
                 lon=0.0,
                 alt=0.0,
                 ca=5,
                 tc=11,
                 ss=0,
                 nicsb=0,
                 time=0,
                 surface=False):
        if len(icao) < 3 or len(icao) > 8:
            raise ValueError(
                "_icao_ length should have 9 to 32 bits, between 3 and 8 hexadecimal values ex: 0xABCDEF01"
            )

        self.icao = int(icao, 16)
        self.lat = lat
        self.lon = lon
        self.alt = alt
        self.ca = ca
        self.tc = tc
        self.ss = ss
        self.nicsb = nicsb
        self.time = time
        self.surface = surface

    def __str__(self):
        s = 'ADS-B Message: '
        s += '\r\n   ICAO: 0x' + ''.join(
            format(x, '02x') for x in self.icao.to_bytes(4, 'big')).upper()
        s += '\r\n   Time: %7d  Surv.Status: %1d  Surface: %d' % (
            self.time, self.ss, self.surface)
        s += '\r\n   Lat: %2.6f Lon: %2.6f Alt: %4.1f' % (self.lat, self.lon,
                                                          self.alt)
        s += '\r\n   Capability: %2d    Type code: %2d    NIC: %1d' % (
            self.ca, self.tc, self.nicsb)
        s += '\r\n'
        return s


def byte_array_to_frame(byte_array_even, byte_array_odd=b'', period=1.0):
    """
    Convert byte array to 'ADS-B Frame' adding preamble without checking payload

    Parameters
    ----------
    byte_array_even : bytearray
        Payload to the even frame
    byte_array_odd : bytearray
        Payload to the odd frame
    period : float
        Pause time to the next frame ('zeros at the end')
    Returns
    -------
    list
        a list of floats in PPM
    """

    # Convert Byte array to PPM
    df17_array = adbs_encoder.frame_1090es_ppm_modulate(
        byte_array_even, byte_array_odd, period)

    # Convert PPM to Float array of "raw bits"
    samples_array = adbs_encoder.hackrf_raw_IQ_format(df17_array)

    # Scale float samples to GNU
    scale_factor = 1.4142135623730951  # sqrt(2)
    scaled_array = [i * scale_factor for i in samples_array]

    return scaled_array


def info_to_adsb_frame(frame, period, output_file):
    """
    Convert a list of ADS-B information to a valid ADS-B Frame and save it to a .dat file

    Parameters
    ----------
    frame : list of AdsbFrame
        ADS-B information for each frame
    period : list of float
        Pause at the end of the corresponding frame
    output_file : str
        Output filename with path and extension (.dat)
    """

    output_array = []

    if len(frame) < 1 or len(period) != len(period):
        raise ValueError(
            "_frame_ and _period_ should have the same length and not None")

    for f, p in zip(frame, period):

        df17_even, df17_odd = adbs_encoder.df17_pos_rep_encode(
            f.ca, f.icao, f.tc, f.ss, f.nicsb, f.alt, f.time, f.lat, f.lon,
            f.surface)

        # print(''.join(format(x, '02x') for x in df17_even))
        # print(''.join(format(x, '02x') for x in df17_odd))

        output_array += byte_array_to_frame(df17_even, period=p)

    #print(output_array)

    # Export data to GNU .dat file
    export_dat(output_array, fileout=output_file)


def bytes_to_adsb_frame(data, period, output_file):
    """
    Convert a list of bytearray to 'ADS-B Frame' adding preamble (without checking the payload)
    and save it to a .dat file

    Parameters
    ----------
    data : list of bytearray
        ADS-B payload for each frame
    period : list of float
        Pause at the end of the corresponding frame
    output_file : str
        Output filename with path and extension (.dat)
    """

    output_array = []

    if len(data) < 1 or len(data) != len(period):
        raise ValueError(
            "_data_ and _period_ should have the same length and not None")

    for d, p in zip(data, period):
        if not isinstance(d, bytes):
            raise ValueError("_data_ should be of type 'bytes'")
        output_array += byte_array_to_frame(d, period=p)

    # print(output_array)

    # Export data to GNU .dat file
    export_dat(output_array, fileout=output_file)


def byte_array_to_bit_array(buff):
    """
    Convert a bytearray to array of bits

    Parameters
    ----------
    buff : bytearray
        Data to convert

    Returns
    -------
    bits : list of int
        List of binary data
    """

    # bytes to bit array (char)
    bits_s = ''.join(f'{byte:08b}' for byte in buff)

    bits = []
    for b in bits_s:
        bits.append(int(b))

    return bits


def import_dat(file):
    """
    Import a .dat file as a list of floats

    Parameters
    ----------
    file : str
        File path to the file

    Returns
    -------
    floats : list of float
        Data imported in a list of floats
    """
    floats = []
    with open(file, 'rb') as f:
        bytes = f.read()

        # IEEE float are 4 bytes long, divide in chunks of 4 bytes
        ieee_floats = [bytes[i:i + 4] for i in range(0, len(bytes), 4)]

        # convert each 4 bytes to float
        for f in ieee_floats:
            val = float(struct.unpack('<f', f)[0])
            floats.append(val)

        # print(floats)
    return floats


def export_dat(floats, fileout='data.dat'):
    """
    Export a list of floats to a .dat file

    Parameters
    ----------
    floats : list of float
        Data to export
    fileout : str
        Output filename with path and extension (.dat)
    """

    byte_array = bytearray()
    for f in floats:
        # Convert each float in 4 bytes (IEEE standard)
        b = struct.pack('<f', f)
        # Append to array
        byte_array += b

    # Save all bytes to output file
    with open(fileout, 'wb') as f:
        f.write(byte_array)


if __name__ == "__main__":
    frame1 = AdsbFrame(b'ABC123',
                       lat=12.34,
                       lon=56.78,
                       alt=1000.0,
                       time=0,
                       ca=5,
                       tc=11,
                       ss=0,
                       nicsb=0,
                       surface=False)
    # frame2 = AdsbFrame(b'123456',
    #                    lat=12.34,
    #                    lon=56.78,
    #                    alt=1000.0,
    #                    time=0,
    #                    ca=5,
    #                    tc=11,
    #                    ss=0,
    #                    nicsb=0,
    #                    surface=False)
    print(frame1)
    # print(frame2)

    info_to_adsb_frame([frame1], [0.00088],
                       output_file='article_1000.dat')
    
    # bytes_to_adsb_frame(
    #     [b'8dabcdef580b003a064bb8c96061'],
    #     [1],
    #     output_file='bytes2adsb_1sec.dat')
    """     
    # See https://mode-s.org/decode/adsb/airborne-position.html

    # Option 1
    # export .dat file from list of hexadecimal bytearray
    bytes_to_adsb_frame(
        [b'8dabcdef5837f03a064bb8549297', b'8dabcdef5837f416effaf7446037'],
        [0.01, 0.5],
        output_file='adsb.dat')

    # OR (RECOMMENDED!)
    # Option 2
    # export .dat file from a list of information
    frame1 = AdsbFrame(b'ABCDEF',
                       lat=12.34,
                       lon=56.78,
                       alt=1000.0,
                       time=0,
                       ca=5,
                       tc=11,
                       ss=0,
                       nicsb=0,
                       surface=False)-
    frame2 = AdsbFrame(b'012345', lat=1.0, lon=2.0, alt=100.0)
    frame3 = AdsbFrame(b'00DEAD', lat=-12.34, lon=-56.78, alt=50.0)
    frame4 = AdsbFrame(b'012345', lat=10.0, lon=20.0, alt=1000.0)

    print(frame1)
    print(frame2)
    print(frame3)
    print(frame4)

    info_to_adsb_frame([frame1, frame2, frame3, frame4], [0.5, 0.5, 0.5, 1],
                       output_file='adsb.dat') 
    
    """

###############################################################
